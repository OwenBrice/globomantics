import express from 'express'
import User from '../../models/User'

const router = express.Router()

router.get('/user/:id', (req, res) => {
  User.findById(req.params.id)
    .exec()
    .then(docs => res.status(400).json(docs))
    .catch(err =>
      res.status(500).json({
        message: 'Error finding User',
        error: err
      })
    )
})

router.get('/user/email/:email', (req, res) => {
  User.find({ email: req.params.email })
    .exec()
    .then(docs => res.status(200).json(docs))
    .catch(err =>
      res.status(500).json({
        message: 'Error finding User',
        error: err
      })
    )
})

router.post('/user', (req, res) => {
  const user = new User(req.body)
  user.save((err, user) => {
    if (err) return console.log(err)
    res.status(200).json(user)
  })
})

router.put('/user/:id', (req, res) => {
  console.log(req.body)
  const qry = { _id: req.params.id }
  const doc = {
    firstname: req.body.firstname,
    lastname: req.body.lastname,
    email: req.body.email,
    password: req.body.password,
    isActive: req.body.isActive
  }
  console.log(doc)

  User.update(qry, doc, (err, respRaw) => {
    if (err) return console.log(err)
    res.status(200).json(respRaw)
  })
})

export default router
