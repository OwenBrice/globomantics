import express from 'express'
import mongoose from 'mongoose'
import Transaction from '../../models/Transaction'

const router = express.Router()

// Get transactions for given year and month, by userId
router.get('/transaction/:year/:month', (req, res) => {
  const userId = req.get('userId')
  const month = req.params.month - 1 // JS months are zero based
  const year = req.params.year
  const startDate = new Date(Date.UTC(year, month, 1, 0, 0, 0))
  const endDate = new Date(Date.UTC(year, month + 1, 1, 0, 0, 0))

  const qry = {
    userId,
    transactionDate: {
      $gte: startDate,
      $lt: endDate
    }
  }

  Transaction.find(qry)
    .sort({ transaction: 1 })
    .exec()
    .then(docs => res.status(200).json(docs))
    .catch(err =>
      res.status(500).json({
        message: 'Error finding transactions for user',
        error: err
      })
    )
})

// Get transactions running balance for specific user
router.get('/transaction/balance/:year/:month', (req, res) => {
  const userId = req.get('userId')
  const month = req.params.month - 1 // JS months are zero based
  const year = req.params.year
  const endDate = new Date(Date.UTC(year, month + 1, 1, 0, 0, 0))

  const pipeline = [
    {
      $match: {
        userId: mongoose.Types.ObjectId(userId)
      }
    },
    {
      $match: {
        transactionDate: { $lt: endDate }
      }
    },
    {
      $group: {
        _id: null,
        charges: { $sum: '$charge' },
        deposits: { $sum: '$deposit' }
      }
    }
  ]

  Transaction.aggregate(pipeline)
    .exec()
    .then(docs => res.status(200).json(docs))
    .catch(err =>
      res.status(500).json({
        message: 'Error finding transactions for user',
        error: err
      })
    )
})

// Create new transaction document
router.post('/transaction', (req, res) => {
  const transaction = new Transaction(req.body)
  transaction.save((err, transaction) => {
    if (err) return console.log(err)
    res.status(200).json(transaction)
  })
})

export default router
