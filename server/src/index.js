import express from 'express'
import morgan from 'morgan'
import bodyParser from 'body-parser'
import mongoose from 'mongoose'
import cors from 'cors'
//import api from './api'
import users from './api/routes/user'
import transactions from './api/routes/transaction'
import { mongoURI } from './config/keys'

const app = express()

app.set('port', process.env.PORT || 8081)

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: false }))

app.use(cors())
// app.use('api', api)
// app.use(express.static('static'))
// Use Routes
app.use('/api', users)
app.use('/api', transactions)

app.use(morgan('dev'))

app.use((req, res, next) => {
  const err = new Error('Not Found')
  err.status = 404
  res.status(404).json(err)
})

mongoose.connect(mongoURI, { useNewUrlParser: true })

const db = mongoose.connection

db.on('error', console.error.bind(console, 'connection error:'))
db.once('open', () => {
  console.log('Connected to MongoDB')

  app.listen(app.get('port'), () => {
    console.log('API Server Listening on port ' + app.get('port'))
  })
})
