import Vue from 'vue'
import bcrypt from 'bcryptjs'

const state = {
  email: '',
  userId: null,
  firstname: '',
  lastname: '',
  isLoggedIn: false,
  loginError: ''
}

const getters = {
  isLoggedIn: state => state.isLoggedIn,
  userId: state => state.userId,
  loginError: state => state.loginError
}

const actions = {
  async loginUser ({ commit }, payload) {
    await Vue.axios
      .get('/user/email/' + payload.email)
      .then(resp => {
        const data = resp.data
        if (data && data.length > 0) {
          // Test password entered (paylod) against user object
          const pwdHash = data[0].password
          if (bcrypt.compareSync(payload.password, pwdHash)) {
            const user = data[0]
            payload.userId = user._id
            payload.firstname = user.firstname
            payload.lastname = user.lastname
            payload.email = user.email
            commit('loginUser', payload)
          } else {
            commit('loginError')
          }
        }
      })
      .catch(() => {
        commit('loginError')
      })
  },

  updateUserProfile ({ commit }, payload) {
    bcrypt.hash(payload.password, 8, (err, hash) => {
      if (!err) {
        payload.password = hash
        Vue.axios
          .put('/user/' + this.state.user.userId, payload)
          .then(resp => {
            console.log(resp)
          })
          .catch(err => {
            console.log(err)
          })
      }
    })
  }
}

const mutations = {
  loginUser (state, payload) {
    state.firstname = payload.firstname
    state.lastname = payload.lastname
    state.email = payload.email
    state.userId = payload.userId
    state.isLoggedIn = true
  },

  loginError (state) {
    state.isLoggedIn = false
    state.loginError = 'Email and/or Password are invalid. Login failed.'
  }
}

export default {
  state,
  getters,
  actions,
  mutations
}
