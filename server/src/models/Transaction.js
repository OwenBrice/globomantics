import mongoose, { Schema, SchemaTypes } from 'mongoose'

const userTransaction = new Schema({
  userId: SchemaTypes.ObjectId,
  transactionDate: {
    type: Date,
    required: true
  },
  transactionType: {
    type: String,
    required: true
  },
  description: {
    type: String,
    required: true
  },
  charge: {
    type: Number,
    default: 0
  },
  deposit: {
    type: Number,
    default: 0
  },
  notes: {
    type: String,
    default: ''
  },
  createdAt: {
    type: Date,
    default: Date.now
  }
})

const Transaction = mongoose.model('Transaction', userTransaction)

export default Transaction
